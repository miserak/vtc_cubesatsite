<?php 
 $title = 'CubedOS';
 $revision_date = 'December 20, 2019';
 ?>

<!DOCTYPE html>
<html lang="en">
  <?php require_once('../includes/header.php'); ?>
  <body>
    <header class="sticky-top">
      <?php require_once('../includes/nav.php'); ?>
    </header>
    <?php require_once('../includes/banner.php'); ?>

    <main role="main" class="container">
      <?php require_once('./overview.html'); ?>
    </main>

    <?php require_once('../includes/footer.php'); ?>
  </body>
</html>
