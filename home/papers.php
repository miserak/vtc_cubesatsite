<?php 
 $cubesat_file_server = 'http://www.cubesatlab.org:430/PUBLIC';
?>

<h2>Papers and Presentations</h2>

<ul>
  <li>
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/brandon-chapin-farnsworth-klink-AUJ-2017.pdf" . '"'; ?>>
	CubedOS: A Verified CubeSat OperatingSystem
      </a>
    </strong>
    by Carl Brandon, Peter Chapin, Chris Farnsworth, and
    Sean Klink. <i>Ada User Journal</i> 38(3), September
    2017.
  </li>

  <li>
    SPARK/Frama-C Day, presentation by Carl Brandon and
    Peter Chapin, May 30, 2017, Paris,
    France. (
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/SPARK-Frama-C-Day-2017.pdf" . '"'; ?>> Slides
      </a>
    </strong>)
  </li>

  <li>
    CubeSat Developers Workshop, presentation by Carl
    Brandon, April 28, 2017.  (
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/CubeSat-Developers-Workshop-2017.pdf" . '"'; ?>> Slides
    </a></strong>
    )
  </li>

  <li>
    STEM Academy Lecture Series at Essex High School,
    presentation by Peter Chapin, November 17, 2016. (
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/Essex-High-School-2016-11-17.pdf" . '"'; ?>> Slides
      </a>
    </strong>
    )
  </li>

  <li>
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/brandon-chapin-HILT-2016.pdf" . '"'; ?>> The Use of SPARK
	in a Complex Spacecraft
      </a>
    </strong>
    by Carl Brandon and Peter Chapin.
    <i>
      Proceedings of the
      <strong>
	<a <?php echo 'href="' . $cubesat_file_server
	  . "http://www.sigada.org/conf/hilt2016/" . '"'; ?>> High
	  Integrity Language Technology workshop
	</a>
      </strong>
    </i>
    (HILT-2016); October 6-7, 2016, Pittsburgh, PA, USA. (
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/2016-10-06-HILT-2016.pdf" . '"'; ?>> Slides
      </a>
    </strong>
    )
  </li>

  <li>
    Spacecraft Flight Software Workshop (FSW-2015), Laurel,
    MD, October 29, 2015 (
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/2015-10-29-FSW-2015.pdf" . '"'; ?>> Slides
      </a>
    </strong>
    )
  </li>

  <li>
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/brandon-chapin-IAC-2015.pdf" . '"'; ?>> High Integrity
	Software for CubeSats and Other Space Missions
      </a>
    </strong>
    by Carl Brandon and Peter Chapin.
    <i>
      Proceedings of
      <strong>
	<a href="https://web.archive.org/web/20160822141102/http://www.iac2015.org/">
	  66<sup>th</sup>
	  International Astronautical Congress
	</a>
      </strong>
    </i>
    ; October 12--16, 2015, Jerusalem, Israel (
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
      . "/2015-10-15-IAC-2015.pdf" . '"'; ?>> Slides
      </a>
    </strong>
    )
  </li>

  <li>
    Vermont Space Grant Consortium dinner, September 16,
    2015 (
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/Space-Grant-Dinner-2015-Poster.pdf" . '"'; ?>> Poster
      </a>
    </strong>
    ).
  </li>

  <li>
    STEM Academy Lecture Series at Essex High School,
    presentation by Peter Chapin, October 28, 2014. (
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/Essex-High-School-2014-10-28.pdf" . '"'; ?>> Slides
      </a>
    </strong>
    )
  </li>

  <li>
    Vermont Space Grant Consortium dinner, November 4, 2013 (
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/Space-Grant-Dinner-2013-Poster.pdf" . '"'; ?>> Poster
      </a>
    </strong>)
    .
  </li>

  <li>
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/brandon-chapin-AdaEurope2013.pdf" . '"'; ?>> A SPARK/Ada
	CubeSat Control Program
      </a>
    </strong>
    by Carl Brandon and Peter Chapin.
    <i>
      Proceedings of
      <strong>
	<a href="http://www.ada-europe2013.org/">
	  Ada Europe 2013
	</a>
      </strong>
    </i>
    ; June 10--14, 2013 (Berlin, Germany); LNCS 7896; pages
    51--64. (
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/AdaEurope2013.pdf" . '"'; ?>> Slides
      </a>
    </strong>
    )
  </li>

  <li>
    CubeSat Developers Workshop 2013, presentation by Carl
    Brandon, April 2013. (
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/CubeSat-Developers-Workshop-2013.pdf" . '"'; ?>> Slides
      </a>
    </strong>
    )
  </li>

  <li>
    Senior Projects presentation on the flight control
    software by Dan Turner and Colin Myers, April
    2013. (
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/Senior-Projects-2013-Flight-Control-System.pdf" . '"'; ?>>
	Slides
      </a>
    </strong>
    )
  </li>

  <li>
    Vermont Space Grant Consortium dinner, November 7, 2012
    <ul>
      <li>
	<strong>
	  <a <?php echo 'href="' . $cubesat_file_server
	    . "/Space-Grant-Dinner-2012-Poster.pdf" . '"'; ?>> CubeSat
	    Flight Control Software
	  </a>
	</strong>
      </li>
      <li>
	<strong>
	  <a <?php echo 'href="' . $cubesat_file_server
	    . "/Space-Grant-Dinner-2012-Slides.pdf" . '"'; ?>>
	    Presentation Slides
	  </a>
	</strong>
      </li>
    </ul>
  </li>

  <li>
    Senior Projects presentation on the navigation system by
    Al Corkery, April 2011. (
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/Senior-Projects-2011-CubeSat-Navigation-System.pdf" . '"';
	?>> Slides
      </a>
    </strong>
    )
  </li>

  <li>
    Senior Projects presentation on the ground station by
    Jordan Hodge, Jordan Lyford, and Wilson Schreiber, April
    2011. (
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/Senior-Projects-2011-Ground-Station.pdf" . '"'; ?>> Slides
      </a>
    </strong>
    )
  </li>

  <li>
    Vermont Space Grant Consortium dinner, October 6, 2010
    <ul>
      <li>
	<strong>
	  <a <?php echo 'href="' . $cubesat_file_server
	    . "/Space-Grant-Dinner-2010-Poster-CubeSat-Lunar-Lander-Design.pdf"
	    . '"'; ?>> Lunar Lander Design
	  </a>
	</strong>
      </li>
      <li>
	<strong>
	  <a <?php echo 'href="' . $cubesat_file_server
	    . "/Space-Grant-Dinner-2010-Poster-Ion-Drive-Lunar-Orbiter.pdf"
	    . '"'; ?>> Ion Drive Lunar Orbiter
	  </a>
	</strong>
      </li>
      <li>
	<strong>
	  <a <?php echo 'href="' . $cubesat_file_server
	    . "/Space-Grant-Dinner-2010-Poster-Software-Development-Process.pdf"
	    . '"'; ?>> Software Development Process
	  </a>
	</strong>
      </li>
      <li>
	<strong>
	  <a <?php echo 'href="' . $cubesat_file_server
	    . "/Space-Grant-Dinner-2010-Poster-SPARK-Ada-for-CubeSats.pdf"
	    . '"'; ?>> SPARK/Ada for CubeSats
	  </a>
	</strong>
      </li>
    </ul>
  </li>

  <li>
    CubeSat Developers Workshop 2010, presentation by Carl
    Brandon, April 23, 2010. (
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/CubeSat-Developers-Workshop-2010.pdf" . '"'; ?>> Slides
      </a>
    </strong>
    )
  </li>

  <li>
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/loseby-chapin-brandon-SIGAda2009.pdf" . '"'; ?>> Use of
	SPARK in a Resource Constrained Embedded System
      </a>
    </strong>
    by Chad Loseby, Peter Chapin, and
    Carl Brandon.
    <i>
      Proceedings of
      <strong>
	<a <?php echo 'href="' . $cubesat_file_server
	  . "http://www.sigada.org/conf/sigada2009/" . '"'; ?>> SIGAda
	  2009
	</a>
      </strong>
    </i>
    ; November 1--5, 2013 (St. Petersburg, Florida); pages
    87--90. (
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server . "/SIGAda2009.pdf" . '"'; ?>>
	Slides
      </a>
    </strong>
    )
  </li>

  <li>
    Senior Projects presentation on the Alaskan ice buoy
    software by Chad Loseby, April 2009. (
    <strong>
      <a <?php echo 'href="' . $cubesat_file_server
	. "/Senior-Projects-2009-Alaskan-Ice.pdf" . '"'; ?>> Slides
      </a>
    </strong>
    )
  </li>
</ul>
