<?php 
 $title = 'VTC CubeSat Lab';
 $revision_date = 'January 5, 2021';
 ?>

<!DOCTYPE html>
<html lang="en">
  <?php require_once('./includes/header.php'); ?>
  <body>
    <header class="sticky-top">
      <?php require_once('./includes/nav.php'); ?>
    </header>
    <?php require_once('./includes/banner.php'); ?>

    <main role="main" class="container">
      <?php require_once('./home/overview.html'); ?>
      <hr>
      <?php require_once('./home/papers.php'); ?>
      <hr>
      <?php require_once('./home/news.html'); ?>
      <hr>
      <?php require_once('./home/people.html'); ?>
      <hr>
      <?php require_once('./home/acknowledgments.html'); ?>
    </main>

    <?php require_once('./includes/footer.php'); ?>
  </body>
</html>
