<div id="navigationbar">
  <div class="navbar navbar-expand-md bg-dark navbar-dark">
    <a href="/" class="navbar-brand">VTC CubeSat Laboratory</a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="collapsibleNavbar">
      <!-- Links -->
      <ul class="navbar-nav">
	<li class="nav-item">
	  <a href="/" class="nav-link">Home</a>
        </li>
        <li class="nav-item">
	  <a href="/cubedos/" class="nav-link">CubedOS</a>
        </li>

        <!-- BasicLEO Dropdown -->
        <li class="nav-item dropdown">
	  <a href="#" class="nav-link dropdown-toggle" 
             id="navbardrop" data-toggle="dropdown">
            BasicLEO
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="/basic-leo/">Overview</a>
            <a class="dropdown-item" href="/basic-leo/hardware/">Hardware</a>
            <a class="dropdown-item" href="/basic-leo/software/">Software</a>
            <a class="dropdown-item" href="/basic-leo/pictures/">Pictures</a>
            <a class="dropdown-item" href="/basic-leo/data">Data</a>
          </div>
        </li>

        <!-- AlaskanIce Link -->
        <li class="nav-item">
          <a href="/alaskan-ice/" class="nav-link">AlaskanIce</a>
        </li>
      </ul>
    </div>
  </div>
</div>
