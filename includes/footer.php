<?php
  $year = date("Y");
?>

<footer class="jumbotron-fluid jumbotron text-center">
<p>
&#169; Copyright 2015 - <?php echo $year; ?> by Vermont Technical College | Last
Revised: <?php echo $revision_date; ?>
</p>
</footer>
